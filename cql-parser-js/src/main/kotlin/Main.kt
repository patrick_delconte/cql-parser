import ch.umb.solutions.curo.cql.PredictionResult
import ch.umb.solutions.curo.cql.predictNextToken

@JsName("predict")
fun predict(text: String?, knownWords: Array<String> = emptyArray()): PredictionResult {
    if (text.isNullOrEmpty()) return PredictionResult(emptyArray(), false)
    return predictNextToken(text!!, knownWords.toList())
}