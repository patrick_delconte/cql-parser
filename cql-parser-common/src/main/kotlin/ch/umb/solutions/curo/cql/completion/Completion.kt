package ch.umb.solutions.curo.cql.completion

import generated.CuroQueryLanguageBaseListener
import generated.CuroQueryLanguageLexer
import generated.CuroQueryLanguageParser
import org.antlr.v4.kotlinruntime.BaseErrorListener
import org.antlr.v4.kotlinruntime.RecognitionException
import org.antlr.v4.kotlinruntime.Recognizer


class TextValueCompletionListener(private val knownWords: List<String>) : CuroQueryLanguageBaseListener() {
    var lastMatchForKnownWords = emptyList<String>()

    override fun exitTextValue(ctx: CuroQueryLanguageParser.TextValueContext) {
        lastMatchForKnownWords = match(ctx.text, knownWords)
    }
}

class IncompleteStatementsCompletionListener : BaseErrorListener() {
    var isValid = true
        private set
    val candidates = ArrayList<String>()

    override fun syntaxError(
        recognizer: Recognizer<*, *>,
        offendingSymbol: Any?,
        line: Int,
        charPositionInLine: Int,
        msg: String,
        e: RecognitionException
    ) {
        // syntax error
        isValid = false

        val expectedTokens = e.expectedTokens
        val text = e.offendingToken?.text

        if (expectedTokens == null || text == null) return


        val tokens = CuroQueryLanguageLexer.Tokens.values().filter { expectedTokens.contains(it.id) }
        for (token in tokens) {
            candidates.addAll(
                when (token) {
                    CuroQueryLanguageLexer.Tokens.CAMUNDA_DATE_TOKEN -> match(text, CAMUNDA_DATE_TOKEN)
                    CuroQueryLanguageLexer.Tokens.CAMUNDA_TOKEN -> match(text, CAMUNDA_TOKEN)
                    else -> emptyList()
                }
            )
        }
    }
}

//TODO could use levenshteindist from raifaisenproject here
private fun match(text: String, candidates: List<String>) = candidates.filter { it.contains(text) }


//TODO these should really be read from the grammar instead
private val CAMUNDA_TOKEN = listOf(
    "processInstanceId",
    "processInstanceBusinessKey",
    "processDefinitionId",
    "processDefinitionKey",
    "processDefinitionName",
    "executionId",
    "caseInstanceId",
    "caseInstanceBusinessKey",
    "caseDefinitionId",
    "caseDefinitionKey",
    "caseDefinitionName",
    "caseExecutionId",
    "withoutTenantId",
    "assignee",
    "owner",
    "candidateGroup",
    "candidateUser",
    "includeAssignedTasks",
    "involvedUser",
    "assigned",
    "unassigned",
    "taskDefinitionKey",
    "name",
    "description",
    "priority",
    "maxPriority",
    "minPriority",
    "delegationState",
    "candidateGroups",
    "withCandidateGroups",
    "withoutCandidateGroups",
    "active",
    "suspended",
    "taskVariables",
    "processVariables",
    "caseInstanceVariables",
    "parentTaskId",
    "sortBy",
    "sortOrder",
    "firstResult",
    "maxResults"
)

private val CAMUNDA_DATE_TOKEN = listOf("due", "followUp", "created")