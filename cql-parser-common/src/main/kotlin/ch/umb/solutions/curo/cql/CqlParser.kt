package ch.umb.solutions.curo.cql


import ch.umb.solutions.curo.cql.completion.IncompleteStatementsCompletionListener
import ch.umb.solutions.curo.cql.completion.TextValueCompletionListener
import com.strumenta.kotlinmultiplatform.Arrays
import generated.CuroQueryLanguageLexer
import generated.CuroQueryLanguageListener
import generated.CuroQueryLanguageParser
import org.antlr.v4.kotlinruntime.ANTLRErrorListener
import org.antlr.v4.kotlinruntime.ANTLRInputStream
import org.antlr.v4.kotlinruntime.CommonTokenStream

fun parse(
    cqlQuery: String,
    listeners: List<CuroQueryLanguageListener> = emptyList(),
    errorListeners: List<ANTLRErrorListener> = emptyList()
): CuroQueryLanguageParser.SearchTermListContext {

    val input = ANTLRInputStream(cqlQuery)
    val lexer = CuroQueryLanguageLexer(input)
    val tokens = CommonTokenStream(lexer)
    val parser = CuroQueryLanguageParser(tokens)

    // add listeners
    listeners.forEach { parser.addParseListener(it) }
    errorListeners.forEach { parser.addErrorListener(it) }


    parser.buildParseTree = true

    return parser.searchTermList()
}

/**
 * @param cqlQuery for which to predict the next possible words
 * @param knownWords a list of additional words that are taken into consideration for the prediction.
 * You can use this to supply words that would be legit in some context. Currently not supported
 */
fun predictNextToken(cqlQuery: String, knownWords: List<String> = emptyList()): PredictionResult {

    val input = ANTLRInputStream(cqlQuery)
    val lexer = CuroQueryLanguageLexer(input)
    val tokenStream = CommonTokenStream(lexer)
    val parser = CuroQueryLanguageParser(tokenStream)
    val parseCompletionListener = TextValueCompletionListener(knownWords)
    val incompleteStatementsCompletionListener = IncompleteStatementsCompletionListener()

    parser.addParseListener(parseCompletionListener)
    parser.addErrorListener(incompleteStatementsCompletionListener)
    parser.searchTermList() // start parsing

    val candidates = incompleteStatementsCompletionListener.candidates
    candidates.addAll(parseCompletionListener.lastMatchForKnownWords)

    return PredictionResult(candidates.toTypedArray(), incompleteStatementsCompletionListener.isValid)
}


data class PredictionResult(val candidates: Array<String>, val isValid: Boolean) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as PredictionResult

        if (!Arrays.equals(candidates, other.candidates)) return false
        if (isValid != other.isValid) return false

        return true
    }

    override fun hashCode(): Int {
        var result = 31 * isValid.hashCode()
        for (candidate in candidates) {
            result = 31 * result + candidate.hashCode()

        }
        return result
    }
}

