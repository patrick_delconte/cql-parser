// Generated from java-escape by ANTLR 4.7.1
package generated

import org.antlr.v4.kotlinruntime.tree.ParseTreeListener

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CuroQueryLanguageParser}.
 */
interface CuroQueryLanguageListener : ParseTreeListener {
    /**
     * Enter a parse tree produced by {@link CuroQueryLanguageParser#identifier}.
     * @param ctx the parse tree
     */
    fun enterIdentifier(ctx: CuroQueryLanguageParser.IdentifierContext)

    /**
     * Exit a parse tree produced by {@link CuroQueryLanguageParser#identifier}.
     * @param ctx the parse tree
     */
    fun exitIdentifier(ctx: CuroQueryLanguageParser.IdentifierContext)

    /**
     * Enter a parse tree produced by {@link CuroQueryLanguageParser#numericValue}.
     * @param ctx the parse tree
     */
    fun enterNumericValue(ctx: CuroQueryLanguageParser.NumericValueContext)

    /**
     * Exit a parse tree produced by {@link CuroQueryLanguageParser#numericValue}.
     * @param ctx the parse tree
     */
    fun exitNumericValue(ctx: CuroQueryLanguageParser.NumericValueContext)

    /**
     * Enter a parse tree produced by {@link CuroQueryLanguageParser#dateValue}.
     * @param ctx the parse tree
     */
    fun enterDateValue(ctx: CuroQueryLanguageParser.DateValueContext)

    /**
     * Exit a parse tree produced by {@link CuroQueryLanguageParser#dateValue}.
     * @param ctx the parse tree
     */
    fun exitDateValue(ctx: CuroQueryLanguageParser.DateValueContext)

    /**
     * Enter a parse tree produced by {@link CuroQueryLanguageParser#textValue}.
     * @param ctx the parse tree
     */
    fun enterTextValue(ctx: CuroQueryLanguageParser.TextValueContext)

    /**
     * Exit a parse tree produced by {@link CuroQueryLanguageParser#textValue}.
     * @param ctx the parse tree
     */
    fun exitTextValue(ctx: CuroQueryLanguageParser.TextValueContext)

    /**
     * Enter a parse tree produced by {@link CuroQueryLanguageParser#textComparator}.
     * @param ctx the parse tree
     */
    fun enterTextComparator(ctx: CuroQueryLanguageParser.TextComparatorContext)

    /**
     * Exit a parse tree produced by {@link CuroQueryLanguageParser#textComparator}.
     * @param ctx the parse tree
     */
    fun exitTextComparator(ctx: CuroQueryLanguageParser.TextComparatorContext)

    /**
     * Enter a parse tree produced by {@link CuroQueryLanguageParser#numericComparator}.
     * @param ctx the parse tree
     */
    fun enterNumericComparator(ctx: CuroQueryLanguageParser.NumericComparatorContext)

    /**
     * Exit a parse tree produced by {@link CuroQueryLanguageParser#numericComparator}.
     * @param ctx the parse tree
     */
    fun exitNumericComparator(ctx: CuroQueryLanguageParser.NumericComparatorContext)

    /**
     * Enter a parse tree produced by {@link CuroQueryLanguageParser#dateComparator}.
     * @param ctx the parse tree
     */
    fun enterDateComparator(ctx: CuroQueryLanguageParser.DateComparatorContext)

    /**
     * Exit a parse tree produced by {@link CuroQueryLanguageParser#dateComparator}.
     * @param ctx the parse tree
     */
    fun exitDateComparator(ctx: CuroQueryLanguageParser.DateComparatorContext)

    /**
     * Enter a parse tree produced by {@link CuroQueryLanguageParser#range}.
     * @param ctx the parse tree
     */
    fun enterRange(ctx: CuroQueryLanguageParser.RangeContext)

    /**
     * Exit a parse tree produced by {@link CuroQueryLanguageParser#range}.
     * @param ctx the parse tree
     */
    fun exitRange(ctx: CuroQueryLanguageParser.RangeContext)

    /**
     * Enter a parse tree produced by {@link CuroQueryLanguageParser#numericRange}.
     * @param ctx the parse tree
     */
    fun enterNumericRange(ctx: CuroQueryLanguageParser.NumericRangeContext)

    /**
     * Exit a parse tree produced by {@link CuroQueryLanguageParser#numericRange}.
     * @param ctx the parse tree
     */
    fun exitNumericRange(ctx: CuroQueryLanguageParser.NumericRangeContext)

    /**
     * Enter a parse tree produced by {@link CuroQueryLanguageParser#dateRange}.
     * @param ctx the parse tree
     */
    fun enterDateRange(ctx: CuroQueryLanguageParser.DateRangeContext)

    /**
     * Exit a parse tree produced by {@link CuroQueryLanguageParser#dateRange}.
     * @param ctx the parse tree
     */
    fun exitDateRange(ctx: CuroQueryLanguageParser.DateRangeContext)

    /**
     * Enter a parse tree produced by {@link CuroQueryLanguageParser#term}.
     * @param ctx the parse tree
     */
    fun enterTerm(ctx: CuroQueryLanguageParser.TermContext)

    /**
     * Exit a parse tree produced by {@link CuroQueryLanguageParser#term}.
     * @param ctx the parse tree
     */
    fun exitTerm(ctx: CuroQueryLanguageParser.TermContext)

    /**
     * Enter a parse tree produced by {@link CuroQueryLanguageParser#searchterm}.
     * @param ctx the parse tree
     */
    fun enterSearchterm(ctx: CuroQueryLanguageParser.SearchtermContext)

    /**
     * Exit a parse tree produced by {@link CuroQueryLanguageParser#searchterm}.
     * @param ctx the parse tree
     */
    fun exitSearchterm(ctx: CuroQueryLanguageParser.SearchtermContext)

    /**
     * Enter a parse tree produced by {@link CuroQueryLanguageParser#searchTermList}.
     * @param ctx the parse tree
     */
    fun enterSearchTermList(ctx: CuroQueryLanguageParser.SearchTermListContext)

    /**
     * Exit a parse tree produced by {@link CuroQueryLanguageParser#searchTermList}.
     * @param ctx the parse tree
     */
    fun exitSearchTermList(ctx: CuroQueryLanguageParser.SearchTermListContext)
}