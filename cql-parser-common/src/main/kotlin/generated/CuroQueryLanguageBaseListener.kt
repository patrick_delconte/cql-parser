// Generated from java-escape by ANTLR 4.7.1
package generated

import org.antlr.v4.kotlinruntime.ParserRuleContext
import org.antlr.v4.kotlinruntime.tree.ErrorNode
import org.antlr.v4.kotlinruntime.tree.TerminalNode

/**
 * This class provides an empty implementation of {@link CuroQueryLanguageListener},
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
open class CuroQueryLanguageBaseListener : CuroQueryLanguageListener {
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterIdentifier(ctx: CuroQueryLanguageParser.IdentifierContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitIdentifier(ctx: CuroQueryLanguageParser.IdentifierContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterNumericValue(ctx: CuroQueryLanguageParser.NumericValueContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitNumericValue(ctx: CuroQueryLanguageParser.NumericValueContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterDateValue(ctx: CuroQueryLanguageParser.DateValueContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitDateValue(ctx: CuroQueryLanguageParser.DateValueContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterTextValue(ctx: CuroQueryLanguageParser.TextValueContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitTextValue(ctx: CuroQueryLanguageParser.TextValueContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterTextComparator(ctx: CuroQueryLanguageParser.TextComparatorContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitTextComparator(ctx: CuroQueryLanguageParser.TextComparatorContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterNumericComparator(ctx: CuroQueryLanguageParser.NumericComparatorContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitNumericComparator(ctx: CuroQueryLanguageParser.NumericComparatorContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterDateComparator(ctx: CuroQueryLanguageParser.DateComparatorContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitDateComparator(ctx: CuroQueryLanguageParser.DateComparatorContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterRange(ctx: CuroQueryLanguageParser.RangeContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitRange(ctx: CuroQueryLanguageParser.RangeContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterNumericRange(ctx: CuroQueryLanguageParser.NumericRangeContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitNumericRange(ctx: CuroQueryLanguageParser.NumericRangeContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterDateRange(ctx: CuroQueryLanguageParser.DateRangeContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitDateRange(ctx: CuroQueryLanguageParser.DateRangeContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterTerm(ctx: CuroQueryLanguageParser.TermContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitTerm(ctx: CuroQueryLanguageParser.TermContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterSearchterm(ctx: CuroQueryLanguageParser.SearchtermContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitSearchterm(ctx: CuroQueryLanguageParser.SearchtermContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterSearchTermList(ctx: CuroQueryLanguageParser.SearchTermListContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitSearchTermList(ctx: CuroQueryLanguageParser.SearchTermListContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun enterEveryRule(ctx: ParserRuleContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun exitEveryRule(ctx: ParserRuleContext) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun visitTerminal(node: TerminalNode) {}

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    override fun visitErrorNode(node: ErrorNode) {}
}