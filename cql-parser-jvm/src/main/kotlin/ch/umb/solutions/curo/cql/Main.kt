package ch.umb.solutions.curo.cql

import generated.CuroQueryLanguageBaseListener
import generated.CuroQueryLanguageParser
import org.antlr.v4.kotlinruntime.ConsoleErrorListener


fun main(args: Array<String>) {
    val dummyListener = object : CuroQueryLanguageBaseListener() {
        override fun exitIdentifier(ctx: CuroQueryLanguageParser.IdentifierContext) {
            println("exit identifier ${ctx.text}")
        }
    }

    val parsed = parse("assignee like chris", listOf(dummyListener), listOf(ConsoleErrorListener()))

    println(predictNextToken("ass"))
    println(predictNextToken("assignee "))
    println(predictNextToken("assignee ="))
    println(predictNextToken("assignee like chris", listOf("christopher.kelley", "superman")))
    println(predictNextToken("assignee like chris and name like sup", listOf("christopher.kelley", "superman")))

    println("done")
}